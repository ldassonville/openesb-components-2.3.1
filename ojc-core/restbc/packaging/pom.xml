<?xml version="1.0" encoding="UTF-8"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)pom.xml
 # Copyright 2006-2008 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->
<project>
    <parent>
        <artifactId>package-common</artifactId>
        <groupId>open-jbi-components</groupId>
        <version>1.1</version>
        <relativePath>../../build-common/package-common</relativePath>
    </parent>
    <modelVersion>4.0.0</modelVersion>
    <groupId>open-jbi-components</groupId>
    <artifactId>restbc-installer</artifactId>
    <name>sun-rest-binding</name>
    <version>${restbc.artifact.version}</version>
    <description>Creates an installable jar for the Rest Binding Component</description>
    <build>
        <resources>
            <resource>
                <targetPath>META-INF</targetPath>
                <directory>src</directory>
                <filtering>true</filtering>
                <includes>
                    <include>*.xml</include>
                </includes>
            </resource>
            <resource>
                <targetPath>./</targetPath>
                <directory>src</directory>
                <filtering>false</filtering>
                <includes>
                    <include>config.properties</include>
                    <include>*.jks</include>
                </includes>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <artifactId>maven-antrun-plugin</artifactId>
                <executions>
                    <execution>
                        <id>${project.artifactId}-generate-i18n-descriptors</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <tasks>
                                <!-- generate i18n jbi descriptors -->
                                <taskdef name="i18n-xml" classname="${I18N_XML_TASK_CLASS}" classpath="${I18NTASK_JAR}" />
                                <i18n-xml
                                    file="${project.build.sourceDirectory}/jbi.xml"
                                    properties="${project.build.sourceDirectory}/restbc-config.properties"
                                    outputDir="${project.build.outputDirectory}/META-INF">
                                    <fileset dir="${project.build.sourceDirectory}"
                                             includes="restbc-config*.properties"/>
                                    <namespace prefix="jbi" uri="http://java.sun.com/xml/ns/jbi"/>
                                    <namespace prefix="cfg" uri="http://www.sun.com/jbi/Configuration/V1.0"/>
                                    <expr query="//cfg:Property"
                                          i18nAttr="displayName" keyAttr="name"/>
                                    <expr query="//cfg:Property"
                                          i18nAttr="displayDescription" keyAttr="name"/>
                                    <expr query="/jbi:jbi/jbi:component/jbi:identification/jbi:description"/>
                                </i18n-xml>
                            </tasks>
                        </configuration>
                    </execution>
                    <execution>
                        <id>${project.artifactId}-rename-jars</id>
                        <phase>process-classes</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <tasks>
                                <!-- rename jbiadapter jar -->
                                <move
                                    file="${project.build.outputDirectory}/lib/restjbiadapter.jar"
                                    tofile="${project.build.outputDirectory}/lib/restbc-jbiadapter.jar" />
                            </tasks>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <id>${project.artifactId}-fetch-deps</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>copy-dependencies</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.outputDirectory}/lib</outputDirectory>
                            <stripVersion>true</stripVersion>
                            <excludeTransitive>true</excludeTransitive>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-jar-plugin</artifactId>
                <configuration>
                    <archive>
                        <manifestFile>${project.build.outputDirectory}/META-INF/MANIFEST.MF</manifestFile>
                    </archive>

                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.felix</groupId>
                <artifactId>maven-bundle-plugin</artifactId>
                <executions>
                    <execution>
                        <id>bundle-manifest</id>
                        <phase>process-classes</phase>
                        <goals>
                            <goal>manifest</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <instructions>
                        <Bundle-SymbolicName>sun-rest-binding</Bundle-SymbolicName>
                        <Bundle-Name>REST Binding Component</Bundle-Name>
                        <Bundle-Description>Implementation of Rest Binding Component</Bundle-Description>
                        <Bundle-Version>${OSGI_VERSION}</Bundle-Version>
                        <_removeheaders>${OSGI_MANIFEST_REMOVE_HEADERS}, Export-Package</_removeheaders>
                        <Export-Package>
                            *;-noimport:=true
                        </Export-Package>
                        <Bundle-ClassPath>
                            lib/restbc-jbiadapter.jar,
                            lib/componentsl.jar,
                            lib/common-util.jar,
                            lib/wsdl4j.jar,
                            lib/wsdl4jext.jar,
                            lib/customized-xmlbeans.jar,
                            lib/resolver.jar,
                            lib/qos.jar,
			                lib/jersey-bundle.jar,
			                lib/grizzly-servlet-webserver.jar,
			                lib/asm.jar,
                            lib/jackson-core-asl.jar,
                            lib/jackson-mapper-asl.jar,
			                lib/jsr311-api.jar,
                            lib/net.sf.hulp.meas.itf.jar,
                            lib/net.sf.hulp.meas.impl.jar			    
                        </Bundle-ClassPath>
                        <Import-Package>
                            javax.activation.*,
                            javax.jbi.*,
                            javax.mail.*,
                            javax.management.*,
                            javax.naming.*,
                            javax.net.ssl.*,
                            javax.servlet;version="2.5",
                            javax.servlet.http;version="2.5",
                            javax.servlet.jsp;resolution:=optional,
                            javax.servlet.resources;resolution:=optional;version="2.5",
                            javax.xml.namespace,
                            javax.xml.parsers.*,
                            javax.xml.transform.*,
                            javax.xml.xpath.*,
                            org.osgi.framework.*,
                            org.w3c.dom.*,
                            org.xml.sax.*,
                            org.glassfish.openesb.api.message,
                            org.glassfish.openesb.api.service
                        </Import-Package>
                        <Bundle-Activator>com.sun.jbi.restbc.jbiadapter.inbound.ComponentBundleActivator</Bundle-Activator>
                        <!--
                        <DynamicImport-Package>*</DynamicImport-Package>
                        -->
                    </instructions>
                </configuration>

            </plugin>
        </plugins>
    </build>
    <dependencies>
        <dependency>
            <groupId>open-jbi-components</groupId>
            <artifactId>restjbiadapter</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>open-jbi-components</groupId>
            <artifactId>wsdl4j</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>open-jbi-components</groupId>
            <artifactId>wsdl4jext</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>open-jbi-components</groupId>
            <artifactId>customized-xmlbeans</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>open-jbi-components</groupId>
            <artifactId>resolver</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>open-jbi-components</groupId>
            <artifactId>componentsl</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>open-jbi-components</groupId>
            <artifactId>qos</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>open-jbi-components</groupId>
            <artifactId>common-util</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>com.sun.jersey</groupId>
            <artifactId>jersey-bundle</artifactId>
            <optional>true</optional>
            <version>1.0.3.1</version>
        </dependency>	
        <dependency>
          <groupId>com.sun.grizzly</groupId>
          <artifactId>grizzly-servlet-webserver</artifactId>
          <optional>true</optional>
          <version>1.9.10</version>
        </dependency>
        <dependency>
          <groupId>javax.ws.rs</groupId>
          <artifactId>jsr311-api</artifactId>
          <optional>true</optional>
          <version>1.0</version>
        </dependency>
        <dependency>
          <groupId>asm</groupId>
          <artifactId>asm</artifactId>
          <optional>true</optional>
          <version>3.1</version>
        </dependency>
        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-core-asl</artifactId>
            <optional>true</optional>
            <version>1.9.5</version>
        </dependency>
        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-mapper-asl</artifactId>
            <optional>true</optional>
            <version>1.9.5</version>
        </dependency>
        <dependency>
            <groupId>net.sf.hulp.meas</groupId>
            <artifactId>net.sf.hulp.meas.itf</artifactId>
            <optional>true</optional>
            <version>2.1-SNAPSHOT</version>
        </dependency>
        <dependency>
            <groupId>net.sf.hulp.meas</groupId>
            <artifactId>net.sf.hulp.meas.impl</artifactId>
            <optional>true</optional>
            <version>2.1-SNAPSHOT</version>
        </dependency>
    </dependencies>
</project>
